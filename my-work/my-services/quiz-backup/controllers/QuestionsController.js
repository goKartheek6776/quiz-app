var restful = require('node-restful');
module.exports = function(app, route) {
	console.log('entered');
    var rest = restful.model('question', app.models.question).methods(['get']);
    rest.register(app, route);
    return function(req, res, next) { next(); };
}