var mongoose = require('mongoose');
const QuestionsSchema = new mongoose.Schema({
	QuestionNumber: {
		type: String,
	},

	QuestionType: {
		type: String,
	},

	Question: {
		type: String,
	},

	Options: {
		type: JSON,
	},

	Answer: {
		type: String,
	}

});

module.exports = QuestionsSchema;